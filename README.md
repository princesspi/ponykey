# ponykey

A secure webchat with end-to-end encryption the the highest possible security and anonymity standard. Uses web standards where possible.

## features

- [ ] Auto-generated identity
  - [ ] Truncated BIP39 (Mnemonic), defaults to first 3 words
  - [ ] Color hash
- [ ] Message Signing
- [ ] Message Encryption
- [x] Runs as a local binary
- [ ] Built-in ngrok for private instances
  - Disable ngrok with NODE_ENV=production
- [ ] Favicon
- [ ] Private rooms
- [ ] Spam prevention in #lobby

## build

### prereqs

- NodeJS 12+
- yarn - `npm i -g yarn`

### install modules

- `cd ponykey`
- `yarn`

### build commands

To see a full list of build commands, run `yarn run`.

For a standalone build, use:

- `yarn compile`

For development, use:

- `yarn watch`

## usage

Environment variables are used to change program options. They are:

- `CHAT_NAME` - Default: `PonyKey Chat`
- `NODE_ENV` - Default: ``
- `SERVER_PORT` - Default: `3000`

## development

PonyKey uses:

- NodeJS
- TypeScript
- Webpack
- React
  - React Hooks
- TypeStyle
- W3C APIs
  - Web Authentication
  - Web Crypto
  - Credential Management
