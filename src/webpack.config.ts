import * as path from 'path'
import * as webpack from 'webpack'

const entry = path.resolve(__dirname, 'web', 'index.js')
const output = path.resolve(__dirname, 'dist')

const NODE_ENV = process.env.NODE_ENV || 'development'

console.log(NODE_ENV)

export default {
  mode: NODE_ENV,
  entry,
  output: {
    filename: 'main.js',
    path: output,
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': JSON.stringify(NODE_ENV),
    }),
  ],
}
