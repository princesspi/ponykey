import { Buffer } from 'buffer'

const { NODE_ENV, SERVER_PORT, CHAT_NAME } = process.env

// Environment
export const chatName = CHAT_NAME || 'PonyKey Chat'
export const isProd = NODE_ENV === 'production'
export const PORT = SERVER_PORT || 3000

// Endpoints

export const endpoint = {
  browser: `ws://localhost:${PORT}/browser`,
  server: `ws://localhost:${PORT}/server`,
}

// Serialization
export enum Topic {
  Chat_Message = 'chat/message',
}

export enum Endpoint {
  Browser = 'Browser',
  Server = 'Server',
}

export interface ActionEvent<T> {
  endpoint: Endpoint
  topic: Topic
  payload: T
}

export const formatPrefix = (endpoint: Endpoint, topic: Topic) =>
  Buffer.from(JSON.stringify([endpoint, topic])).slice(0, -2) // removes "] for topic generalization

export const encodeActionEvent = <T>(
  endpoint: Endpoint,
  topic: Topic,
  payload: T,
) => JSON.stringify([endpoint, topic, payload])

export const decodeActionEvent = <T>(msg: Buffer): ActionEvent<T> => {
  const [endpoint, topic, payload] = JSON.parse(msg.toString('utf-8')) as [
    Endpoint,
    Topic,
    T,
  ]
  return { endpoint, topic, payload }
}

// Message Types
export interface ChatMessage {
  id: string
  user: string
  text: string
  time: number
}
